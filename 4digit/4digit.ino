
const int SEG_A = 2;
const int SEG_B = 3;
const int SEG_C = 4;
const int SEG_D = 5;
const int SEG_E = 7;
const int SEG_F = 8;
const int SEG_G = 12;
const int SEG_PUNT = 13;

const int LCD_1 = 11;
const int LCD_2 = 10;
const int LCD_3 = 9;
const int LCD_4 = 6;

const int AANTAL_DISPLAYS = 4;
const int PIN_VAN_DISPLAY[AANTAL_DISPLAYS] = {11, 10, 9, 6};

const int SEC = 1000;
const int AANTAL_SEGMENTEN = 7;
const int AANTAL_SYMBOLEN = 14;
const int PIN_VAN_SEGMENT[AANTAL_SEGMENTEN] = {SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G};
const int SEGMENT_STATUS_PER_SYMBOOL[AANTAL_SYMBOLEN][AANTAL_SEGMENTEN] = {
//{ A,    B,    C,    D,    E,    F,    G  },  //segment
  { HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, LOW }, //0
  { LOW,  HIGH, HIGH, LOW,   LOW, LOW,  LOW }, //1
  { HIGH, HIGH, LOW,  HIGH, HIGH, LOW,  HIGH}, //2
  { HIGH, HIGH, HIGH, HIGH, LOW,  LOW,  HIGH}, //3
  { LOW,  HIGH, HIGH, LOW,  LOW,  HIGH, HIGH}, //4 
  { HIGH, LOW,  HIGH, HIGH, LOW,  HIGH, HIGH}, //5 
  { HIGH, LOW,  HIGH, HIGH, HIGH, HIGH, HIGH}, //6 
  { HIGH, HIGH, HIGH, LOW,  LOW,  LOW,  LOW }, //7  
  { HIGH, HIGH, HIGH, HIGH, HIGH, HIGH, HIGH}, //8 
  { HIGH, HIGH, HIGH, HIGH, LOW,  HIGH, HIGH}, //9 
  { LOW,  LOW,  LOW,  LOW,  LOW,  LOW,  LOW }, //uit
  { LOW,  HIGH, HIGH, LOW, HIGH, HIGH, HIGH }, //H 
  { LOW,  LOW,  LOW,  HIGH, HIGH, HIGH, HIGH }, //t
  { HIGH, HIGH, LOW,  LOW,  HIGH, HIGH, HIGH} //P  
  };
const int S_0 = 0;
const int S_1 = 1;
const int S_2 = 2;
const int S_3 = 3;
const int S_4 = 4;
const int S_5 = 5;
const int S_6 = 6;
const int S_7 = 7;
const int S_8 = 8;
const int S_9 = 9;
const int S_BL = 10;
const int S_H = 11;
const int S_t = 12;
const int S_P = 13;

const int K_THIJS = 1;
const int K_HANNE = 0;
const int K_PAPA = 2;

const int WACHTTIJD = 1;

const int DISP_AAN = LOW;
const int DISP_UIT = HIGH;

const int KNOP_WISSEL = 0;
const int KNOP_RESET = 1;
const int AANTAL_KNOPPEN = 2;
const int KNOP_PIN[AANTAL_KNOPPEN] = { A0, A1 };

const int AANTAL_TIJDEN = 3;

int knop_state[AANTAL_KNOPPEN] = {0, 0};

int symbolen[AANTAL_DISPLAYS] = {S_1, S_2, S_3, S_4 };
boolean dubbelpunt = true;

void toonPunt(boolean aan) {
  digitalWrite(SEG_PUNT, aan ? HIGH:LOW);
}

void schrijf_symbool(int nb) {
  int symbool = nb;
  if (nb < 0 || nb > AANTAL_SYMBOLEN-1) symbool = S_BL;
  for (int segmentnr = 0; segmentnr < AANTAL_SEGMENTEN; segmentnr++) {
    digitalWrite(PIN_VAN_SEGMENT[segmentnr], SEGMENT_STATUS_PER_SYMBOOL[symbool][segmentnr]);
  }
  digitalWrite(SEG_PUNT, dubbelpunt? HIGH:LOW);
}

void wis(){
  schrijf_symbool(S_BL);
} 


void toon_display(int disp) {
  for (int i = 0; i < AANTAL_DISPLAYS; i++) {
    digitalWrite(PIN_VAN_DISPLAY[i], i == disp ? DISP_AAN: DISP_UIT);
  }
}

void schrijf_symbolen() {
  for (int disp = 0; disp < AANTAL_DISPLAYS; disp++) {
    wis();
    toon_display(disp);
    schrijf_symbool(symbolen[disp]);
    delay(WACHTTIJD);
  }
}

void zet_symbolen(int s0, int s1, int s2, int s3) {
  symbolen[0] = s0;
  symbolen[1] = s1;
  symbolen[2] = s2;
  symbolen[3] = s3;
}

int symbool = 0;
unsigned long afteltijd[AANTAL_TIJDEN] = {0, 0, 0};
int tijdkeuze = 1;

void zet_symbool(int s) {
  symbool = s;
  zet_symbolen(s, s, s, s);
}

void verhoog_symbool() {
  zet_symbool(symbool < 9 ? symbool+1:0);
}

void zet_tijd(unsigned long tijd) {
  afteltijd[tijdkeuze] = millis() + tijd;
}

void switch_tijd() {
  tijdkeuze = (tijdkeuze + 1)%AANTAL_TIJDEN; 
}

void schrijf_tijd(unsigned long millies) {
  long alleseconden = millies / 1000;
  long minuten = alleseconden / 60;
  long seconden = alleseconden % 60;
  int eersteLetter = S_BL;
  switch(tijdkeuze) {
    case K_THIJS: eersteLetter = S_t; break;
    case K_HANNE: eersteLetter = S_H; break;
    case K_PAPA : eersteLetter = S_P; break;
    default: eersteLetter = S_BL;
  }
  zet_symbolen(eersteLetter, minuten % 10, seconden / 10, seconden % 10);
}

 

void setup() {
  for(int i = 0; i <= 13; i++) {
    pinMode(i, OUTPUT);
  }

  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  digitalWrite(A3, OUTPUT);
  digitalWrite(A4, OUTPUT);

  //cijfers
  digitalWrite(LCD_1,LOW); //cijfer 1
  digitalWrite(LCD_2,LOW); //cijfer 2
  digitalWrite(LCD_3, LOW); //cijfer 3
  digitalWrite(LCD_4, LOW); //cijfer 4

  //led
  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);

  zet_tijd(0);
}

boolean was_knop_ingedrukt(int knop) {
  boolean state = digitalRead(KNOP_PIN[knop]);
  boolean vorige_state = knop_state[knop];
  boolean ingedrukt =  (state && (state != vorige_state));
  knop_state[knop] = state;
  return ingedrukt;
}

void update_tijd() {
  unsigned long huidige_tijd = millis();
  if (huidige_tijd < afteltijd[tijdkeuze]) {
    schrijf_tijd(afteltijd[tijdkeuze] - huidige_tijd);
  } else {
    schrijf_tijd(0);
  }
}

void update_led() {
  switch(tijdkeuze) {
    case K_HANNE: digitalWrite(0, HIGH); digitalWrite(1, LOW); break;
    case K_THIJS: digitalWrite(0, LOW); digitalWrite(1, HIGH); break;
    case K_PAPA: digitalWrite(0, LOW); digitalWrite(1, LOW); break;
    default: digitalWrite(0, LOW); digitalWrite(0, LOW); break;
  }
  
  digitalWrite(tijdkeuze, HIGH);
  digitalWrite((tijdkeuze + 1)%2, LOW);
}

void loop() {
  update_tijd();
  schrijf_symbolen();
  if (was_knop_ingedrukt(KNOP_RESET)) zet_tijd(121000);
  if (was_knop_ingedrukt(KNOP_WISSEL)) switch_tijd();
  update_led();
  
}
